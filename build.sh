#!/bin/bash

echo "compiling tasker"
rm -rf /tmp/gronx
git clone https://github.com/adhocore/gronx.git /tmp/gronx

cp  build2/build.sh /tmp/gronx/cmd/tasker
cd /tmp/gronx/cmd/tasker
chmod +x /tmp/gronx/cmd/tasker/build.sh
./build.sh tasker 1.8.1